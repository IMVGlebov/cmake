clang-tidy-prefer-p-option
--------------------------

* If :prop_tgt:`<LANG>_CLANG_TIDY` includes a ``-p`` argument, the
  full compiler command line is no longer appended after ``--``.
