cmake-E-env-modify
------------------

* A new ``--modify`` flag was added to :option:`cmake -E env <cmake-E env>` to
  support :prop_test:`ENVIRONMENT_MODIFICATION` operations.
